package com.example.users.exceptions;

/**
 * Clase de exepciones asociadas a reglas de negocio
 */
public class BusinessException extends Exception{

    private static final long serialVersionUID = 3794733081404597110L;

    /**
     * Contructor
     * @param message
     */
    public BusinessException(String message) {
        super(message);
    }

}
