package com.example.users.controller;


import com.example.users.entities.Respuesta;
import com.example.users.exceptions.BusinessException;
import com.example.users.service.interfaces.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller que expone endpoints relacionado a la autentificación del usuario
 */
@RestController(value = "/authenticate")
@AllArgsConstructor
public class AuthenticateController {

    @Autowired
    IUserService service;

    /**
     * Recibe datos de inicio de sesión para un usuario ya registrado en el sistema
     *
     * @param username correo electrónico del usuario
     * @param password contraseña del usuario
     * @return JWT del usuario logeado o mensaje de error según corresponda
     */
    @GetMapping(
            value = "/authenticate",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(
            @RequestHeader(value = "username") String username,
            @RequestHeader(value = "password") String password) {


        try {

            return ResponseEntity.ok(service.obtenerUsuario(username, password));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }


    }


    /**
     * Cierra la sesión del usuario destruyendo el JWT asociado a este
     *
     * @param jwt JWT del usuario a "cerrar sesión"
     * @return Response Entity con mensaje de estado de la solicitud
     */
    @PostMapping(
            value = "/authenticate",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> logout(
            @RequestHeader(value = "jwt") String jwt) {

        try {

            return ResponseEntity.ok(service.logout(jwt));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());
        }
    }


}
