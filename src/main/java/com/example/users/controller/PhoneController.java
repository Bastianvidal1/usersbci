package com.example.users.controller;


import com.example.users.dto.PhoneDTO;
import com.example.users.entities.Respuesta;
import com.example.users.exceptions.BusinessException;
import com.example.users.service.interfaces.IPhoneService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller que expone endpoints relacionados al CRUD de telefono de un usuario
 */
@RestController("/phone")
@AllArgsConstructor
public class PhoneController {

    @Autowired
    IPhoneService service;


    /**
     * Obtiene los teléfonos asociados a un usuario
     *
     * @param jwt JWT del usuario a filtrar
     * @return Teléfonos registrados en la base de datos para un usuario
     */
    @GetMapping(value = "/phone",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> obtenerTelefonosUsuario(
            @RequestHeader(value = "jwt") String jwt) {

        try {

            return ResponseEntity.ok(service.obtenerTelefonosUsuario(jwt));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }
    }


    /**
     * Ingresa nuevo teléfono para un usuario
     *
     * @param jwt      JWT del usuario al cual agregar el teléfono
     * @param phoneDTO PhoneDTO en formato JSON del nuevo teléfono para el usuario
     * @return Mensaje de estado del proceso
     */
    @PostMapping(value = "/phone",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> ingresarTelefonoUsuario(
            @RequestHeader(value = "jwt") String jwt,
            @RequestBody PhoneDTO phoneDTO) {


        try {

            return ResponseEntity.ok(service.ingresarTelefono(jwt, phoneDTO));

        }catch (BusinessException b){

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }

    }


    /**
     * Remueve un teléfono asociado a un usuario
     *
     * @param jwt  JWT del usuario al cual se le removerá el teléfono
     * @param phoneDTO PhoneDTO en formato JSON del teléfono a remover usuario (Requiere del id del teléfono)
     * @return Mensaje de estado del proceso
     */
    @DeleteMapping(value = "/phone",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removerTelefonoUsuario(
            @RequestHeader(value = "jwt") String jwt,
            @RequestBody PhoneDTO phoneDTO) {

        try {


            return ResponseEntity.ok(service.removerTelefono(jwt, phoneDTO));

        }catch (BusinessException b){

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }


    }


}
