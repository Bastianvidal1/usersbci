package com.example.users.controller;

import com.example.users.dto.UserDTO;
import com.example.users.entities.Respuesta;
import com.example.users.exceptions.BusinessException;
import com.example.users.service.interfaces.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Controller que expone endpoints relacionados al CRUD de usuarios del sistema
 */
@RestController(value = "/user")
@AllArgsConstructor
public class UserController {

    @Autowired
    IUserService service;


    /**
     * Registra un usuario en la base de datos.
     *
     * @param user UserDTO, en formato json a través de un request
     * @return ResponseEntity con el resultado de la consulta
     */
    @PostMapping(
            value = "/user",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> registrarUsuario(
            @RequestBody UserDTO user) {

        try {

            return ResponseEntity.ok(service.registrarUsuario(user));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }


    }


    /**
     * Obtiene la data del usuario desde la base de datos
     *
     * @param jwt JWT del usuario a obtener
     * @return ResponseEntity con la data del usuari
     */
    @GetMapping(
            value = "/user",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> obtenerDataUsuario(
            @RequestHeader(value = "jwt") String jwt) {


        try {

            return ResponseEntity.ok(service.obtenerUsuario(jwt));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }


    }


    /**
     * Modifica un usuario en la base de datos
     *
     * @param user UserDTO, en formato json a través de un request
     * @param jwt  JWT del usuario a modificar
     * @return Response Entity con mensajes de estado
     */
    @PutMapping(
            value = "/user",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> modificarUsuario(
            @RequestBody UserDTO user,
            @RequestHeader(value = "jwt") String jwt) {


        try {
            return ResponseEntity.ok(service.modificarUsuario(user, jwt));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }


    }


    /**
     * Desactiva un usuario en la base de datos
     *
     * @param jwt JWT del usuario a desactivar
     * @return ResponseEntity con el resultado de la consulta
     */
    @PatchMapping(
            value = "/user",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> desactivarUsuario(
            @RequestHeader(value = "jwt") String jwt) {

        try {


            return ResponseEntity.ok(service.desactivarUsuario(jwt));

        } catch (BusinessException b) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Respuesta.builder()
                            .mensaje(b.getMessage())
                            .build());

        }
    }

}
