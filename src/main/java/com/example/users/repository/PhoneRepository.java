package com.example.users.repository;

import com.example.users.entities.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Clase de repositorio para la tabla PhoneDTO
 * - La anotación Repository implica que es una clase con funciones de DAO(Data Acces Object)
 * controlando el acceso a la fuente de datos.
 * - Extiende de JPARepository permitiendo mapear una entidad a la fuente de datos, además
 * de otorgar métodos CRUD por defecto.
 */
@Repository
public interface PhoneRepository  extends JpaRepository<Phone,Integer> {
}
