package com.example.users.util;

import com.example.users.constants.Constantes;
import com.example.users.entities.User;
import com.example.users.exceptions.BusinessException;
import io.jsonwebtoken.*;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


/**
 * Clase de utilidad para la gestión de los JWT de los usuarios.
 */
@Service
public class JwtTokenUtil{

    private String SECRET_KEY = "${jwt.secret}";

    /**
     * Extrae el UUID de usuario con su correspondiente JWT
     *
     * @param token JWT
     * @return El UUID del usuario
     */
    public String extractUUID(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * Extrae la fecha de expiración de un JWT
     *
     * @param token JWT
     * @return Date con la fecha de expiración del JWT
     */
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    /**
     * Extrae los datos del JWT
     *
     * @param token          JWT a extraer
     * @param claimsResolver Function del item a buscar
     * @return Objeto almacenado según el item
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Extrae todos los token JWT almacenados para ser tratados
     *
     * @param token
     * @return
     */
    public Claims extractAllClaims(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * Valida si un token JWT se encuentra expirado
     *
     * @param token JWT a evaluar
     * @return si se encuentra expirado o no
     */
    public boolean isTokenExpired(String token) throws BusinessException {
        try {
            return extractExpiration(token).before(new Date(System.currentTimeMillis()));
        }catch (ExpiredJwtException e){
            throw new BusinessException(Constantes.Mensajes.TOKEN_EXPIRADO);
        }catch (MalformedJwtException e ){
            throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO);
        }catch (Exception e){
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * Genera el token JWT para un usuario
     *
     * @param user objeto de tipo User
     * @return JWT en formato String
     */
    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, user.getUuid());
    }

    /**
     * Encargado de procesar la lógica de creación de un token JWT
     *
     * @param claims  HashMap que almacena los token generados
     * @param subject identificador del usuario con el que se generará el JWT
     * @return JWT en formato String
     */
    public String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 5 )) //5 minutos
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }


    /**
     * Destruye el JWT almacenado según el parámetro, eliminando la persistencia de éste
     *
     * @param token JWT a destruir
     */
    public boolean destroyToken(String token) throws BusinessException {
        try {
            extractAllClaims(token).clear();
            return true;
        }catch (Exception e){
            throw new BusinessException(e.getMessage());
        }
    }

    public boolean expirateToken(String token) throws BusinessException {
        try {
            extractAllClaims(token).setExpiration(new Date(System.currentTimeMillis() - 1000 * 60));
            return true;
        }catch (Exception e){
            throw new BusinessException(e.getMessage());
        }
    }



}
