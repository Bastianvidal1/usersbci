package com.example.users.util;

import com.example.users.constants.Constantes;

import java.util.regex.Matcher;

/**
 * Clase de utilidad para validaciones que se pueden generalizar
 */
public class Utilities {

    /**
     * Valida si una cadena de texto corresponde a un correo electrónico válido
     *
     * @param email Cadena de texto a evaluar
     * @return Si la cadena de texto es válida.
     */
    public static boolean isEmailValid(String email){
        Matcher matcher = Constantes.Regex.EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    /**
     * Valida si una cadena de texto cumple con los requisitos de contraseña designados:
     *  - 1 o más mayúsculas
     *  - 3 o más números
     *  - Contiene minúsculas
     *
     * @param password Cadena de texto a evaluar
     * @return Si la cadena de texto cumple con estos requisitos
     */
    public static boolean isPasswordValid(String password){
        Matcher matcher = Constantes.Regex.PASSWORD_REGEX.matcher(password);
        return matcher.find();
    }
}
