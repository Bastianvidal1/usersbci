package com.example.users.service.impl;


import com.example.users.constants.Constantes;
import com.example.users.dto.PhoneDTO;
import com.example.users.entities.Phone;
import com.example.users.entities.User;
import com.example.users.exceptions.BusinessException;
import com.example.users.repository.PhoneRepository;
import com.example.users.service.interfaces.IPhoneService;
import com.example.users.service.interfaces.IUserService;
import com.example.users.util.JwtTokenUtil;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Clase service de telefono, encargado de aplicar la reglas de negocio y comunicación con la capa de datos
 */
@Service
@AllArgsConstructor
@NoArgsConstructor
public class PhoneService implements IPhoneService {

    @Autowired
    PhoneRepository repository;
    @Autowired
    IUserService userService;
    @Autowired
    JwtTokenUtil jwtTokenUtil;


    /**
     * Ingresa teléfono a la base de datos, invocado al ingresar un nuevo usuario
     *
     * @param objPhone Phone a ingresar para un usuario
     * @return Si se logró ingresar el teléfono
     */
    public boolean ingresarTelefono(Phone objPhone) throws BusinessException {

        try {

            if (objPhone.getUuid_user() == null) {
                return false;
            }

            Phone phone = Phone.builder()
                    .uuid_user(objPhone.getUuid_user())
                    .number(objPhone.getNumber())
                    .citycode(objPhone.getCitycode())
                    .countrycode(objPhone.getCountrycode())
                    .build();

            repository.save(phone);

            return true;
        } catch (Exception e) {

            throw new BusinessException(e.getMessage());

        }
    }


    /**
     * Ingresar un nuevo teléfono a un usuario existente en la base de datos
     *
     * @param phoneDTO PhoneDTO con los datos del nuevo teléfono
     * @param jwt      JWT del usuario al cual se le agregará el nuevo teléfono
     * @return Phone del nuevo teléfono ingresado, validando así su ingreso, null si el JWT es inválido
     */
    public Phone ingresarTelefono(String jwt, PhoneDTO phoneDTO) throws BusinessException {
        try {

            jwtTokenUtil.isTokenExpired(jwt);

            User user = userService.obtenerUsuario(jwt);


            if (user == null) {
                throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO);
            }


            Phone phone = repository.save(Phone.builder()
                    .uuid_user(user.getUuid())
                    .number(phoneDTO.getNumber())
                    .citycode(phoneDTO.getCitycode())
                    .countrycode(phoneDTO.getCountrycode())
                    .build()
            );

            return phone;

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }


    /**
     * Obtiene los teléfonos registrados en la base de datos para un usuario
     *
     * @param jwt JWT del usuario a filtrar
     * @return Lista con los teléfonos registrados para un usuario
     */
    public List<Phone> obtenerTelefonosUsuario(String jwt) throws BusinessException {
        try {


            jwtTokenUtil.isTokenExpired(jwt);

            User user = userService.obtenerUsuario(jwt);

            if (user == null) {
                throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO);
            }

            if (!user.isActive()) {
                throw new BusinessException(Constantes.Mensajes.USUARIO_DESACTIVADO);
            }


            List<Phone> phoneList = repository.findAll()
                    .stream()
                    .filter(p -> p.getUuid_user().equals(user.getUuid()))
                    .collect(Collectors.toList());

            if (phoneList.size() == 0) {
                throw new BusinessException(Constantes.Mensajes.NO_SE_ENCONTRARON_TELEFONOS);
            }

            return phoneList;


        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }


    /**
     * Remueve un telefono asociado a un usuario
     *
     * @param jwt      JWT del usuario al cual se le removerá un teléfono
     * @param phoneDTO PhoneDTO del teléfono a remover (id requerido)
     * @return
     */
    public boolean removerTelefono(String jwt, PhoneDTO phoneDTO) throws BusinessException {

        try {

            jwtTokenUtil.isTokenExpired(jwt);


            User user = userService.obtenerUsuario(jwt);

            if (user == null) {

                throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO);

            }

            repository.delete(Phone.builder()
                    .id(phoneDTO.getId())
                    .build()
            );

            return true;


        } catch (Exception e) {

            throw new BusinessException(e.getMessage());


        }

    }


}
