package com.example.users.service.impl;

import com.example.users.constants.Constantes;
import com.example.users.dto.UserDTO;
import com.example.users.entities.Phone;
import com.example.users.entities.Respuesta;
import com.example.users.entities.User;
import com.example.users.exceptions.BusinessException;
import com.example.users.repository.UserRepository;
import com.example.users.service.interfaces.IPhoneService;
import com.example.users.service.interfaces.IUserService;
import com.example.users.util.JwtTokenUtil;
import com.example.users.util.Utilities;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

/**
 * Clase service de usuario, encargado de aplicar la reglas de negocio y comunicación con la capa de datos.
 */
@Service
@AllArgsConstructor
@NoArgsConstructor
public class UserService implements IUserService {

    @Autowired
    UserRepository repository;
    @Autowired
    IPhoneService phoneService;
    @Autowired
    JwtTokenUtil jwtTokenUtil;


    /**
     * Obtiene el usuario registrado en la base de datos según el UUID
     *
     * @param uuid UUID del usuario a buscar
     * @return User del usuario que coincide con la búsqueda
     */
    public User obtenerUsuarioPorUUID(String uuid) {
        return repository
                .findAll()
                .stream()
                .filter(u -> u.getUuid().equals(uuid))
                .findAny().orElse(null);
    }


    /**
     * Busca en la fuente de datos el usuario que coincida con el email y contraseña
     *
     * @param email    correo electrónico a buscar
     * @param password contraseña a buscar
     * @return JWT generado para el usuario que coincidio con el email y contraseña
     */
    public String obtenerUsuario(String email, String password) throws BusinessException {
        try {
            User user = repository
                    .findAll()
                    .stream()
                    .filter(u -> u.getEmail().equals(email) && u.getPassword().equals(password))
                    .findFirst().orElse(null);


            if (user == null) {
                throw new BusinessException(Constantes.Mensajes.CREDENCIALES_INCORRECTAS);
            }

            if (!user.isActive()) {
                throw new BusinessException(Constantes.Mensajes.USUARIO_DESACTIVADO);
            }

            setLastLogin(user);

            String jwt = jwtTokenUtil.generateToken(user);
            return jwt;

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());

        }

    }


    /**
     * Obtiene un usuario registrado en la base de datos haciendo uso del JWT
     *
     * @param jwt JWT del usuario a obtener
     * @return User del usuario obtenido, null si no se encuentra
     */
    public User obtenerUsuario(String jwt) throws BusinessException {

        try {
            jwtTokenUtil.isTokenExpired(jwt);

            User user = obtenerUsuarioPorUUID(jwtTokenUtil.extractUUID(jwt));

            if (!user.isActive()) {
                throw new BusinessException(Constantes.Mensajes.USUARIO_DESACTIVADO);
            } else {
                return user;
            }

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }

    }


    /**
     * Registra usuarios aplicando creterios de verificación
     *
     * @param userDTO UserDTO con datos del usuario a registrar
     * @return Respuesta con mensajes de estado de la operación
     */
    public Respuesta registrarUsuario(UserDTO userDTO) throws BusinessException {

        try {
            User savedUser;


            if (!Utilities.isEmailValid(userDTO.getEmail())) {
                throw new BusinessException(Constantes.Mensajes.EMAIL_INCORRECTO);
            }

            if (!Utilities.isPasswordValid(userDTO.getPassword())) {
                throw new BusinessException(Constantes.Mensajes.PASSWORD_INCORRECTO);
            }


            if (isExist(userDTO) == null) {

                String uuid = UUID.randomUUID().toString();
                Date date = new Date();

                User user = User.builder()
                        .uuid(uuid)
                        .name(userDTO.getName())
                        .email(userDTO.getEmail())
                        .password(userDTO.getPassword())
                        .created(LocalDateTime.now().format(Constantes.Formatos.dateTimeFormat))
                        .modified(LocalDateTime.now().format(Constantes.Formatos.dateTimeFormat))
                        .last_login(LocalDateTime.now().format(Constantes.Formatos.dateTimeFormat))
                        .token(uuid)//Resolver con JWT
                        .isActive(true)
                        .build();

                savedUser = repository.save(user);

                for (Phone item : userDTO.getPhones()) {
                    item.setUuid_user(uuid);
                    phoneService.ingresarTelefono(item);
                }

                return Respuesta.builder()
                        .mensaje(Constantes.Mensajes.EXITO)
                        .build();

            } else {
                throw new BusinessException(Constantes.Mensajes.EMAIL_YA_REGISTRADO);
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }

    }


    /**
     * Modifica datos de un usuario.
     *
     * @param userDTO UserDTO con nuevos datos
     * @param token   JWT del usuario a modificar, obtenido tras consultar /login
     * @return Respuesta con mensajes de estado.
     */
    public Respuesta modificarUsuario(UserDTO userDTO, String token) throws BusinessException {

        try {

            jwtTokenUtil.isTokenExpired(token);

            String UUID = jwtTokenUtil.extractUUID(token);
            User user = obtenerUsuarioPorUUID(UUID);


            if (!Utilities.isEmailValid(userDTO.getEmail())) {
                throw new BusinessException(Constantes.Mensajes.EMAIL_INCORRECTO);
            }

            if (!Utilities.isPasswordValid(userDTO.getPassword())) {
                throw new BusinessException(Constantes.Mensajes.PASSWORD_INCORRECTO);
            }


            if (!user.isActive()) {
                throw new BusinessException(Constantes.Mensajes.USUARIO_DESACTIVADO);
            }


            user.setName(userDTO.getName());
            user.setPassword(userDTO.getPassword());
            user.setModified(LocalDateTime.now().format(Constantes.Formatos.dateTimeFormat));
            //user.setActive(userDTO.isActive());
            repository.save(user);

            return Respuesta.builder()
                    .mensaje(Constantes.Mensajes.EXITO)
                    .build();


        } catch (Exception e) {

            throw new BusinessException(e.getMessage());

        }

    }

    /**
     * Desactiva usuario de la base de datos
     *
     * @param token JWT del usuario a desactivar, obtenidos tras consultar /login
     * @return Respuesta con mensaje de estado
     */
    public Respuesta desactivarUsuario(String token) throws BusinessException {

        try {

            jwtTokenUtil.isTokenExpired(token);

            String UUID = jwtTokenUtil.extractUUID(token);
            User user = obtenerUsuarioPorUUID(UUID);
            user.setActive(false);
            repository.save(user);

            return Respuesta.builder()
                    .mensaje(Constantes.Mensajes.EXITO)
                    .build();

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());

        }

    }


    /**
     * Valida si existe ya un usuario registrado que coincida con el correo electrónico
     *
     * @param userDTO UserDTO a filtrar
     * @return Si existe un usuario, o null si no se encuentran coincidencias
     */
    public User isExist(UserDTO userDTO) {

        return repository.findAll()
                .stream()
                .filter(u -> u.getEmail().equals(userDTO.getEmail()))
                .findFirst().orElse(null);

    }

    /**
     * Actualiza la última fecha de login del usuario
     *
     * @param user User logeado
     * @return User con fecha de last_login actualizado
     */
    public User setLastLogin(User user) {
        user.setLast_login(LocalDateTime.now().format(Constantes.Formatos.dateTimeFormat));
        return repository.save(user);
    }


    public Respuesta logout(String jwt) throws BusinessException {

        try {

            jwtTokenUtil.isTokenExpired(jwt);

            jwtTokenUtil.destroyToken(jwt);

            return Respuesta.builder()
                    .mensaje(Constantes.Mensajes.EXITO)
                    .build();

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }

}
