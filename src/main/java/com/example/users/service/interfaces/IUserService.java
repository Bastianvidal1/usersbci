package com.example.users.service.interfaces;

import com.example.users.dto.UserDTO;
import com.example.users.entities.Respuesta;
import com.example.users.entities.User;
import com.example.users.exceptions.BusinessException;

public interface IUserService {
    User obtenerUsuarioPorUUID(String uuid);
    String obtenerUsuario(String email, String password) throws BusinessException;
    User obtenerUsuario(String jwt) throws BusinessException;
    Respuesta registrarUsuario(UserDTO userDTO) throws BusinessException;
    Respuesta modificarUsuario(UserDTO userDTO, String token ) throws BusinessException;
    Respuesta desactivarUsuario(String token) throws BusinessException;
    User isExist(UserDTO userDTO);
    User setLastLogin(User user);
    Respuesta logout(String jwt) throws BusinessException;

}
