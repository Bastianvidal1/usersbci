package com.example.users.service.interfaces;

import com.example.users.dto.PhoneDTO;
import com.example.users.entities.Phone;
import com.example.users.exceptions.BusinessException;

import java.util.List;

public interface IPhoneService {


    boolean ingresarTelefono(Phone objPhone) throws BusinessException;
    Phone ingresarTelefono(String jwt, PhoneDTO phoneDTO) throws BusinessException;
    List<Phone> obtenerTelefonosUsuario(String jwt) throws BusinessException;
    boolean removerTelefono(String jwt, PhoneDTO phoneDTO) throws BusinessException;
}
