package com.example.users.constants;

import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

/**
 * Clase que contiene las constantes utilizadas.
 */
public class Constantes {


    /**
     * Contiene formatos de cadenas de texto preestablecidos
     */
    public static class Formatos {
        //Formato para fecha y hora
        public static final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    }

    /**
     * Contiene las constantes asociadas a mensajes
     */
    public static class Mensajes{

        public static final String EXITO = "EXITO";

        public static final String EMAIL_INCORRECTO = "El correo electrónico no es válido.";

        public static final String PASSWORD_INCORRECTO = "La contraseña ingresada no cumple con los requisitos.";

        public static final String EMAIL_YA_REGISTRADO = "El correo ya se encuentra registrado.";

        public static final String CREDENCIALES_INCORRECTAS = "El correo o contraseña son incorrectos.";

        public static final String TOKEN_INVALIDO = "Token inválido.";

        public static final String TOKEN_EXPIRADO = "Token expirado.";

        public static final String USUARIO_DESACTIVADO = "Usuario desactivado.";

        public static final String NO_SE_ENCONTRARON_TELEFONOS = "No se encontraron teléfonos asociados al usuario.";

        public static final String ERROR_DESCONOCIDO = "Se ha producido un error desconocido.";





    }

    /**
     * Contiene las constantes asociadas a patrones REGEX
     */
    public static class Regex{

        //Coincide con cadenas de texto con formato de correo electrónico
        public static final Pattern EMAIL_ADDRESS_REGEX =
                Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", Pattern.CASE_INSENSITIVE);

        //Coincide con cadenas de texto con los requisitos de contraseña:
        //1 mayúscula; 1 o más minúsculas ; 2 o más números
        public static final Pattern PASSWORD_REGEX =
                Pattern.compile("^(?=.{8,})(?=.{2,}[0-9]\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$");

    }



}
