package com.example.users.dto;

import com.example.users.entities.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;


/**
 * Clase modelo para data en formato Json enviada y/o recibida mediante HTTP
 */
@Data
@JsonSerialize
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO{

    private int id;
    private String uuid;
    private String name;
    private String email;
    private String password;

    @JsonProperty("phones")
    private List<Phone> phones;
    private LocalDateTime created;
    private LocalDateTime modified;
    private LocalDateTime last_login;
    private String token;
    private boolean isActive;


}
