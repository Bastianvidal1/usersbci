package com.example.users.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Clase entidad de la tabla PhoneDTO
 */
@Data
@Builder
@JsonSerialize
@NoArgsConstructor
@AllArgsConstructor
public class PhoneDTO {

    private int id;
    private int number;
    private int citycode;
    private int countrycode;

}