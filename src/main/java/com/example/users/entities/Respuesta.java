package com.example.users.entities;

import lombok.*;
import org.springframework.stereotype.Component;

/**
 * Clase utilizada para devolver mensajes de estado en la consulta de endpoints
 */
@Component
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Respuesta{
    private String mensaje;
}
