import com.example.users.UsersApplication
import spock.lang.Specification

/**
 * Clase de testing en Spock para UsersApplication
 * @see UsersApplication
 */
class UsersApplicationTests extends Specification{

    def application;

    def setup(){
        this.application = new UsersApplication();
    }

    def "Inicio aplicación"(){
        when:"Se inicia la aplicación"
            this.application.main()
        then:
            true == true
    }

}
