package Utilities

import com.example.users.entities.User
import com.example.users.exceptions.BusinessException
import com.example.users.util.JwtTokenUtil
import spock.lang.Specification

/**
 * Clase de testing en Spock para JwtTokenUtil
 * @see JwtTokenUtil
 */
class JwtTokenUtilTests extends Specification {

    def jwtUtil
    def result
    def JWT

    def setup() {
        jwtUtil = new JwtTokenUtil()
    }


    def "Generar token"() {
        given: "Un usuario"
        def user = User.builder()
                .uuid(UUID.randomUUID().toString())
                .build()
        when: "Genera un token con usuario"
        result = jwtUtil.generateToken(user)
        then: "Obtiene un JWT"
        result != null
    }

    def "Extraer UUID"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Extrae el UUID de un JWT"
        result = jwtUtil.extractUUID(JWT)
        then: "Obtiene un JWT"
        result != null
    }

    def "Consulta si el JWT se encuentra expirado"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Estado del JWT"
        result = jwtUtil.isTokenExpired(JWT)
        then: "Obtiene si el JWT se encuentra expirado"
        result != null
    }

    def "Consulta si el JWT inválido se encuentra expirado"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Estado del JWT"
        result = jwtUtil.isTokenExpired(null)
        then: "Obtiene una excepción"
        thrown(BusinessException)
    }

    def "Destruye un JWT"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Estado del JWT"
        result = jwtUtil.destroyToken(JWT)
        then: "Obtiene una excepción"
        result == true
    }

    def "Destruye un JWT invalido"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Estado del JWT"
        result = jwtUtil.destroyToken(null)
        then: "Obtiene una excepción"
        thrown(BusinessException)
    }

    def "Expirar un JWT"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Expirar un JWT"
        result = jwtUtil.expirateToken(JWT)
        then: "Obtiene una confirmación"
        result == true
    }

    def "Expirar un JWT invalido"() {
        given: "Un token generado"
        JWT = jwtUtil.generateToken(User.builder()
                .uuid(UUID.randomUUID().toString())
                .build())
        when: "Expirar un JWT"
        result = jwtUtil.expirateToken("JWT")
        then: "Obtiene una excepción"
        thrown(BusinessException)
    }


}
