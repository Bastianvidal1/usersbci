package Utilities

import com.example.users.util.Utilities
import org.junit.Assert
import spock.lang.Specification


/**
 * Clase de testing en Spock para Utilities
 * @see Utilities
 */
class UtilitiesTests extends Specification {

    def utilities


    def setup() {
        this.utilities = new Utilities()
    }

    def "Correo no valido"() {
        given:
        def email = "bvidaltonttdata.com"
        when:
        def result = utilities.isEmailValid(email)
        then:
        Assert.assertEquals(false, result)
    }

    def "Correo valido"() {
        given:
        def email = "bvidalto@nttdata.com"
        when:
        def result = utilities.isEmailValid(email)
        then:
        Assert.assertEquals(true, result)
    }

    def "Contraseña invalida"() {
        given:
        def password = "Bvidal1"
        when:
        def result = utilities.isPasswordValid(password)
        then:
        Assert.assertEquals(false, result)
    }

    def "Contraseña valida"() {
        given:
        def password = "Bvidal12"
        when:
        def result = utilities.isPasswordValid(password)
        then:
        Assert.assertEquals(true, result)
    }

}
