package Service

import com.example.users.dto.UserDTO
import com.example.users.entities.Phone
import com.example.users.entities.Respuesta
import com.example.users.entities.User
import com.example.users.exceptions.BusinessException
import com.example.users.repository.UserRepository
import com.example.users.service.impl.PhoneService
import com.example.users.service.impl.UserService
import com.example.users.util.JwtTokenUtil
import spock.lang.Specification

/**
 * Clase de testing en Spock para UserService
 * @see UserService
 */
class UserServiceTests extends Specification {

    def expiratedJWT = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyOGEwZTQxZS0yYWM5LTQwNDQtYThlNC01NjQ5NWVjMTM1YWUiLCJleHAiOjE2MjIwODY3MTAsImlhdCI6MTYyMjA1MDcxMH0.Wm6GaR9daoe4c5Y_S4r_ZS3cvqWJHwQrq261PwMiqnY"
    def repository = Mock(UserRepository.class)
    def phoneService = Mock(PhoneService.class)
    def jwtUtil = new JwtTokenUtil()
    def service
    def result
    def jwt
    def name = "Bastian Vidal"
    def email = "bastianvidal1998@gmail.com"
    def password = "Bvidal123"

    def setup() {
        this.service = new UserService(repository, phoneService, jwtUtil)
    }


    def "Obtener usuario por UUID "() {
        given: "Consulta del usuario al repositorio"
        List<User> lista = new ArrayList()
        def userUUID = UUID.randomUUID().toString()
        lista.add(User.builder().uuid(userUUID).build())
        this.repository.findAll() >> { lista }
        when: "Consulta al servicio"
        result = this.service.obtenerUsuarioPorUUID(userUUID)
        then: "Obtiene un Usuario"
        result.getClass() == User.class
    }

    def "Obtener usuario con correo y contraseña"() {
        given: "Un usuario válido"
        List<User> lista = new ArrayList<User>()
        def userUUID = UUID.randomUUID().toString()
        lista.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { lista }
        when: "Obtener Usuario"
        result = this.service.obtenerUsuario(email, password)
        then: "Devuelve JWT"
        result != null
    }

    def "Obtener usuario desactivado con correo y contraseña"() {
        given: "Un usuario válido"
        List<User> lista = new ArrayList()
        def userUUID = UUID.randomUUID().toString()
        lista.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(false)
                .build())
        this.repository.findAll() >> { lista }
        when: "Obtener Usuario"
        result = this.service.obtenerUsuario(email, password)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Obtener usuario con correo y contraseña incorrectos"() {
        given: "Un usuario válido"
        List<User> lista = new ArrayList<User>()
        def userUUID = UUID.randomUUID().toString()
        lista.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { lista }
        when: "Obtener Usuario"
        result = this.service.obtenerUsuario("email", "password")
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Obtener usuario con correo incorrecto"() {
        given: "Un usuario válido"
        List<User> lista = new ArrayList<User>()
        def userUUID = UUID.randomUUID().toString()
        lista.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { lista }
        when: "Obtener Usuario"
        result = this.service.obtenerUsuario("email", password)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Obtener usuario con contraseña incorrecta"() {
        given: "Un usuario válido"
        List<User> lista = new ArrayList<User>()
        def userUUID = UUID.randomUUID().toString()
        lista.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { lista }
        when: "Obtener Usuario"
        result = this.service.obtenerUsuario(email, "password")
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Registrar usuario"() {
        given: "Un usuario válido"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        when: "Obtener Usuario"
        result = this.service.registrarUsuario(userDTO)
        then: "Devuelve una respuesta"
        result.getClass() == Respuesta.class
    }


    def "Registrar usuario mail inválido"() {
        given: "Un usuario con correo inválido"
        def userDTO = getUserDTO()
        userDTO.email = "bastian.cl"
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        when: "Obtener Usuario"
        result = this.service.registrarUsuario(userDTO)
        then: "Devuelve un errpr"
        thrown(BusinessException)
    }

    def "Registrar usuario contraseña inválida"() {
        given: "Un usuario con contraseña inválida"
        def userDTO = getUserDTO()
        userDTO.password = "123"
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        when: "Obtener Usuario"
        result = this.service.registrarUsuario(userDTO)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Registrar usuario ya existente"() {
        given: "Un usuario válido"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        list.add(User.builder().email(email).build())
        this.repository.findAll() >> { list }
        when: "Obtener Usuario"
        result = this.service.registrarUsuario(userDTO)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Modificar un usuario"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Modificar usuario"
        result = this.service.modificarUsuario(getUserDTO(), jwt)
        then: "Devuelve una respuesta"
        result.getClass() == Respuesta.class
    }

    def "Modificar un usuario con correo inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Modificar usuario"
        def newUserDTO = getUserDTO()
        newUserDTO.email = "bastian.cl"
        result = this.service.modificarUsuario(newUserDTO, jwt)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Modificar un usuario con contraseña inválida"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Modificar usuario"
        def newUserDTO = getUserDTO()
        newUserDTO.password = "123"
        result = this.service.modificarUsuario(newUserDTO, jwt)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Modificar un usuario desactivado"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Modificar usuario"
        def newUserDTO = getUserDTO()
        list.get(0).active = false
        this.repository.findAll() >> { list }
        result = this.service.modificarUsuario(newUserDTO, jwt)
        then: "Devuelve un error"
        thrown(BusinessException)
    }


    def "Modificar un usuario con JWT inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Modificar usuario"
        def newUserDTO = getUserDTO()
        list = new ArrayList<User>()
        this.repository.findAll() >> { list }
        result = this.service.modificarUsuario(newUserDTO, "jwt")
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Modificar un usuario con JWT expirado"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Modificar usuario"
        def newUserDTO = getUserDTO()
        list = new ArrayList<User>()
        this.repository.findAll() >> { list }
        result = this.service.modificarUsuario(newUserDTO, expiratedJWT)
        then: "Devuelve un error"
        thrown(BusinessException)
    }


    def "Desactivar un usuario"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Desactivar un usuario"
        result = this.service.desactivarUsuario(jwt)
        then: "Devuelve una respuesta"
        result.getClass() == Respuesta.class
    }

    def "Desactivar un usuario con JWT inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Desactivar un usuario"
        result = this.service.desactivarUsuario("jwt")
        then: "Devuelve una respuesta"
        thrown(BusinessException)
    }

    def "Obtener un usuario con JWT"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Obtener un usuario"
        result = this.service.obtenerUsuario(jwt)
        then: "Devuelve una respuesta"
        result.getClass() == User.class
    }

    def "Obtener un usuario desactivado con JWT"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        list.get(0).setActive(false)
        when: "Obtener un usuario"
        result = this.service.obtenerUsuario(jwt)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Obtener un usuario con JWT inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Obtener un usuario"
        result = this.service.obtenerUsuario("jwt")
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Obtener un usuario con JWT expirado"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Obtener un usuario"
        result = this.service.obtenerUsuario(expiratedJWT)
        then: "Devuelve un error"
        thrown(BusinessException)
    }

    def "Logout con JWT"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Logout sesión"
        result = this.service.logout(jwt)
        then: "Devuelve una respuesta"
        result.getClass() == Respuesta.class
    }

    def "Logout con JWT invalido"() {
        given: "Un usuario válido registrado y logeado"
        def userDTO = getUserDTO()
        List<User> list = new ArrayList<>()
        this.repository.findAll() >> { list }
        result = this.service.registrarUsuario(userDTO)
        def userUUID = UUID.randomUUID().toString()
        list.add(User.builder()
                .email(email)
                .password(password)
                .uuid(userUUID)
                .isActive(true)
                .build())
        this.repository.findAll() >> { list }
        jwt = this.service.obtenerUsuario(email, password)
        when: "Logout sesión"
        result = this.service.logout("jwt")
        then: "Devuelve un error"
        thrown(BusinessException)
    }


    Phone getPhone() {
        return Phone.builder()
                .number(31950150)
                .citycode(9)
                .countrycode(56)
                .build();
    }


    List<Phone> getPhoneList() {
        List<Phone> phoneList = new ArrayList<Phone>();
        phoneList.add(getPhone());
        return phoneList;
    }


    UserDTO getUserDTO() {
        return UserDTO.builder()
                .name(name)
                .phones(getPhoneList())
                .email(email)
                .password(password)
                .build();
    }


}
