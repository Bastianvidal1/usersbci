package Service

import com.example.users.dto.PhoneDTO
import com.example.users.dto.UserDTO
import com.example.users.entities.Phone
import com.example.users.entities.User
import com.example.users.exceptions.BusinessException
import com.example.users.repository.PhoneRepository
import com.example.users.service.impl.PhoneService
import com.example.users.service.impl.UserService
import com.example.users.util.JwtTokenUtil
import spock.lang.Specification

/**
 * Clase de testing en Spock para PhoneService
 * @see PhoneService
 */
class PhoneServiceTests extends Specification {

    def expiratedJWT = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyOGEwZTQxZS0yYWM5LTQwNDQtYThlNC01NjQ5NWVjMTM1YWUiLCJleHAiOjE2MjIwODY3MTAsImlhdCI6MTYyMjA1MDcxMH0.Wm6GaR9daoe4c5Y_S4r_ZS3cvqWJHwQrq261PwMiqnY"
    def repository = Mock(PhoneRepository.class)
    def userService = Mock(UserService.class)
    def jwtUtil = new JwtTokenUtil()
    def service
    def result
    def jwt
    def name = "Bastian Vidal"
    def email = "bastianvidal1998@gmail.com"
    def password = "Bvidal123"

    def setup() {
        this.service = new PhoneService(repository, userService, jwtUtil)
    }


    def "Ingresar teléfono para un usuario nuevo"() {
        given: "Un telefono"
        def phone = getPhone()
        phone.uuid_user = UUID.randomUUID().toString()
        this.repository.save() >> { Phone.builder().build() }
        when: "Ingresar un teléfono"
        result = this.service.ingresarTelefono(phone)
        then: "Confirma el ingreso"
        result == true
    }

    def "Ingresar teléfono para un usuario nuevo sin UUID"() {
        given: "Un telefono"
        this.repository.save() >> { Phone.builder().build() }
        when: "Ingresar un teléfono"
        result = this.service.ingresarTelefono(Phone.builder().build())
        then: "Retorna un el rechazo de la operación"
        result == false
    }

    def "Ingresar teléfono nulo para un usuario nuevo"() {
        given: "Un telefono"
        this.repository.save() >> { Phone.builder().build() }
        when: "Ingresar un teléfono"
        result = this.service.ingresarTelefono(null)
        then: "Retorna un error"
        thrown(BusinessException)
    }

    def "Ingresar teléfono para un usuario creado mediante JWT"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        this.userService.obtenerUsuario(jwt) >> { User.builder().uuid(userUUID).build() }
        this.repository.save(_) >> { Phone.builder().build() }
        when: "Ingresar un teléfono"
        result = this.service.ingresarTelefono(jwt, PhoneDTO.builder()
                .number(123)
                .citycode(12)
                .countrycode(12)
                .build())
        then: "Confirma el ingreso"
        result.getClass() == Phone.class
    }

    def "Ingresar teléfono para un usuario creado mediante JWT inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        this.userService.obtenerUsuario(jwt) >> { null }
        this.repository.save(_) >> { Phone.builder().build() }
        when: "Ingresar un teléfono"
        result = this.service.ingresarTelefono(jwt, PhoneDTO.builder()
                .number(123)
                .citycode(12)
                .countrycode(12)
                .build())
        then: "Confirma el ingreso"
        thrown(BusinessException)
    }

    def "Ingresar teléfono para un usuario creado mediante JWT expirado"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        this.userService.obtenerUsuario(jwt) >> { null }
        this.repository.save(_) >> { Phone.builder().build() }
        when: "Ingresar un teléfono"
        result = this.service.ingresarTelefono(expiratedJWT, PhoneDTO.builder()
                .number(123)
                .citycode(12)
                .countrycode(12)
                .build())
        then: "Confirma el ingreso"
        thrown(BusinessException)
    }


    def "Obtener telefonos de un usuario"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(true)
                .build()
        this.userService.obtenerUsuario(jwt) >> { user }
        def phoneList = getPhoneList()
        phoneList.get(0).uuid_user = userUUID
        this.repository.findAll() >> { phoneList }
        when: "Consulta telefonos"
        result = this.service.obtenerTelefonosUsuario(jwt)
        then: "Obtiene el resultado"
        result != null
    }


    def "Obtener telefonos de un usuario sin teléfonos"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(true)
                .build()
        this.userService.obtenerUsuario(jwt) >> { user }
        def phoneList = new ArrayList<Phone>()
        this.repository.findAll() >> { phoneList }
        when: "Ingresar un teléfono"
        result = this.service.obtenerTelefonosUsuario(jwt)
        then: "Devuelve error"
        thrown(BusinessException)
    }


    def "Obtener telefonos de un usuario desactivado"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(false)
                .build()
        this.userService.obtenerUsuario(jwt) >> { user }
        def phoneList = getPhoneList()
        phoneList.get(0).uuid_user = userUUID
        this.repository.findAll() >> { phoneList }
        when: "Ingresar un teléfono"
        result = this.service.obtenerTelefonosUsuario(jwt)
        then: "Devuelve error"
        thrown(BusinessException)
    }


    def "Obtener telefonos de un usuario con JWT inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(true)
                .build()
        this.userService.obtenerUsuario(jwt) >> { null }
        def phoneList = new ArrayList<Phone>()
        this.repository.findAll() >> { phoneList }
        when: "Ingresar un teléfono"
        result = this.service.obtenerTelefonosUsuario(jwt)
        then: "Devuelve error"
        thrown(BusinessException)
    }

    def "Obtener telefonos de un usuario con JWT expirado"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(false)
                .build()
        this.userService.obtenerUsuario(jwt) >> { null }
        def phoneList = getPhoneList()
        phoneList.get(0).uuid_user = userUUID
        this.repository.findAll() >> { phoneList }
        when: "Ingresar un teléfono"
        result = this.service.obtenerTelefonosUsuario(expiratedJWT)
        then: "Devuelve error"
        thrown(BusinessException)
    }


    def "Remover teléfono de un usuario con JWT"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(true)
                .build()
        this.userService.obtenerUsuario(jwt) >> { user }
        def phoneList = getPhoneList()
        phoneList.get(0).uuid_user = userUUID
        this.repository.delete(_) >> { true }
        when: "Remueve un teléfono"
        result = this.service.removerTelefono(jwt, PhoneDTO.builder()
                .number(123)
                .citycode(12)
                .countrycode(12)
                .build())
        then: "Confirma operación"
        result == true
    }

    def "Remover teléfono de un usuario con JWT inválido"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(true)
                .build()
        this.userService.obtenerUsuario(jwt) >> { null }
        def phoneList = getPhoneList()
        phoneList.get(0).uuid_user = userUUID
        this.repository.delete(_) >> { true }
        when: "Remueve un teléfono"
        result = this.service.removerTelefono(jwt, PhoneDTO.builder()
                .number(123)
                .citycode(12)
                .countrycode(12)
                .build())
        then: "Responde una excepción"
        thrown(BusinessException)
    }
    def "Remover teléfono de un usuario con JWT expirado"() {
        given: "Un usuario válido registrado y logeado"
        def userUUID = UUID.randomUUID().toString()
        jwt = jwtUtil.generateToken(User.builder()
                .uuid(userUUID)
                .build())
        def user = User.builder()
                .uuid(userUUID)
                .isActive(true)
                .build()
        this.userService.obtenerUsuario(jwt) >> { null }
        def phoneList = getPhoneList()
        phoneList.get(0).uuid_user = userUUID
        this.repository.delete(_) >> { true }
        when: "Remueve un teléfono"
        result = this.service.removerTelefono(expiratedJWT, PhoneDTO.builder()
                .number(123)
                .citycode(12)
                .countrycode(12)
                .build())
        then: "Responde una excepción"
        thrown(BusinessException)
    }


    Phone getPhone() {
        return Phone.builder()
                .number(31950150)
                .citycode(9)
                .countrycode(56)
                .build();
    }


    List<Phone> getPhoneList() {
        List<Phone> phoneList = new ArrayList<Phone>();
        phoneList.add(getPhone());
        return phoneList;
    }


    UserDTO getUserDTO() {
        return UserDTO.builder()
                .name(name)
                .phones(getPhoneList())
                .email(email)
                .password(password)
                .build();
    }


}
