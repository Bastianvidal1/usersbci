package Controller

import com.example.users.constants.Constantes
import com.example.users.controller.AuthenticateController
import com.example.users.dto.UserDTO
import com.example.users.entities.Phone
import com.example.users.entities.Respuesta
import com.example.users.exceptions.BusinessException
import com.example.users.service.impl.UserService
import org.springframework.http.HttpStatus
import spock.lang.Specification
/**
 * Clase de testing en Spock para AuthenticateController
 * @see AuthenticateController
 */
class AuthenticateControllerTests extends Specification {

    def service = Mock(UserService.class)
    def controller
    def result
    def name = "Bastian Vidal"
    def email = "bastianvidal1998@gmail.com"
    def password = "Bvidal123"

    def setup() {
        this.controller = new AuthenticateController(this.service)
    }

    def "Login Usuario"() {
        given: "Un usuario ya registrado"
        this.service.obtenerUsuario(email, password) >> { "JWT" }
        when: "Login con el correo y contraseña del usuario registrado"
        result = this.controller.login(email, password)
        then: "Devuelve un JWT asociado al usuario logeado"
        result.statusCode == HttpStatus.OK
    }

    def "Login Usuario con credenciales incorrectas"() {
        given: "Un usuario ya registrado"
        this.service.obtenerUsuario(email, password) >> { throw new BusinessException(Constantes.Mensajes.CREDENCIALES_INCORRECTAS) }
        when: "Login con el correo y contraseña incorrectas"
        result = this.controller.login(email, password)
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST
    }


    def "Logout Usuario"() {
        given: "Dado el JWT de un usuario "
        this.service.logout("JWT") >> { Respuesta.builder().mensaje(Constantes.Mensajes.EXITO).build() }
        when: "Logout con el JWT"
        result = this.controller.logout("JWT")
        then: "Confirma loguot del usuario"
        result.statusCode == HttpStatus.OK

    }

    def "Logout Usuario con JWT inválido"() {
        given: "Dado el JWT de un usuario "
        this.service.logout("JWT") >> { throw new BusinessException(Constantes.Mensajes.TOKEN_EXPIRADO) }
        when: "Logout con el JWT incorrecto"
        result = this.controller.logout("JWT")
        then: "Confirma loguot del usuario"
        result.statusCode == HttpStatus.BAD_REQUEST

    }

    Phone getPhone() {
        return Phone.builder()
                .number(31950150)
                .citycode(9)
                .countrycode(56)
                .build();
    }


    List<Phone> getPhoneList() {
        List<Phone> phoneList = new ArrayList<Phone>();
        phoneList.add(getPhone());
        return phoneList;
    }


    UserDTO getUserDTO() {
        return UserDTO.builder()
                .name(name)
                .phones(getPhoneList())
                .email(email)
                .password(password)
                .build();
    }


}
