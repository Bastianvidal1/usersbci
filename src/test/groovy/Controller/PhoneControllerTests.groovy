package Controller

import com.example.users.constants.Constantes
import com.example.users.controller.PhoneController
import com.example.users.dto.PhoneDTO
import com.example.users.dto.UserDTO
import com.example.users.entities.Phone
import com.example.users.exceptions.BusinessException
import com.example.users.service.impl.PhoneService
import org.springframework.http.HttpStatus
import spock.lang.Specification

/**
 * Clase de testing en Spock para PhoneController
 * @see PhoneController
 */
class PhoneControllerTests extends Specification {

    def service = Mock(PhoneService.class)
    def controller
    def result
    def name = "Bastian Vidal"
    def email = "bastianvidal1998@gmail.com"
    def password = "Bvidal123"

    def setup() {
        this.controller = new PhoneController(this.service)
    }


    def "Obtener telefonos usuario"() {
        given: "Telefonos de un usuario válido"
        this.service.obtenerTelefonosUsuario("JWT") >> { getPhoneList() }
        when: "Se obtienen teléfonos de un usuario"
        result = this.controller.obtenerTelefonosUsuario("JWT")
        then: "Devuelve telefonos de un usuario"
        result.statusCode == HttpStatus.OK
    }

    def "Obtener telefonos usuario con JWT inválido"() {
        given: "Telefonos de un usuario con JWT inválido "
        this.service.obtenerTelefonosUsuario("JWT") >> { throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO) }
        when: "Se obtienen teléfonos de un usuario"
        result = this.controller.obtenerTelefonosUsuario("JWT")
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST
    }

    def "Ingresar nuevo teléfono a usuario"() {
        given: "Ingreso de telefonos"
        this.service.ingresarTelefono("JWT", PhoneDTO.builder().build()) >> { getPhone() }
        when: "Se ingres un nuevo telefono para un usuario"
        result = this.controller.ingresarTelefonoUsuario("JWT", PhoneDTO.builder().build())
        then: "Devuelve telefono ingresado"
        result.statusCode == HttpStatus.OK
    }

    def "Ingresar nuevo teléfono a usuario con JWT inválido"() {
        given: "Ingreso de telefonos"
        this.service.ingresarTelefono("JWT", PhoneDTO.builder().build()) >> { throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO) }
        when: "Se ingres un nuevo telefono para un usuario"
        result = this.controller.ingresarTelefonoUsuario("JWT", PhoneDTO.builder().build())
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST
    }


    def "Remueve un teléfono a usuario"() {
        given: "La eliminación de un teléfono"
        this.service.removerTelefono("JWT", PhoneDTO.builder().build()) >> { true }
        when: "Se remueve un teléfono de un usuario"
        result = this.controller.removerTelefonoUsuario("JWT", PhoneDTO.builder().build())
        then: "Devuelve la confirmación"
        result.statusCode == HttpStatus.OK
    }

    def "Remueve un teléfono a usuario con JWT inválido"() {
        given: "La eliminación de un teléfono"
        this.service.removerTelefono("JWT", PhoneDTO.builder().build()) >> { throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO) }
        when: "Se remueve un teléfono de un usuario con JWT inválido"
        result = this.controller.removerTelefonoUsuario("JWT", PhoneDTO.builder().build())
        then: "Devuelve la confirmación"
        result.statusCode == HttpStatus.BAD_REQUEST
    }


    Phone getPhone() {
        return Phone.builder()
                .number(31950150)
                .citycode(9)
                .countrycode(56)
                .build();
    }


    List<Phone> getPhoneList() {
        List<Phone> phoneList = new ArrayList<Phone>();
        phoneList.add(getPhone());
        return phoneList;
    }


    UserDTO getUserDTO() {
        return UserDTO.builder()
                .name(name)
                .phones(getPhoneList())
                .email(email)
                .password(password)
                .build();
    }


}
