package Controller

import com.example.users.constants.Constantes
import com.example.users.controller.UserController
import com.example.users.dto.UserDTO
import com.example.users.entities.Phone
import com.example.users.entities.Respuesta
import com.example.users.entities.User
import com.example.users.exceptions.BusinessException
import com.example.users.service.impl.UserService
import org.springframework.http.HttpStatus
import spock.lang.Specification
/**
 * Clase de testing en Spock para UserController
 * @see UserController
 */
class UserControllerTests extends Specification {

    def service = Mock(UserService.class)
    def controller
    def result
    def name = "Bastian Vidal"
    def email = "bastianvidal1998@gmail.com"
    def password = "Bvidal123"

    def setup() {
        this.controller = new UserController(this.service)
    }

    def "Registrar un nuevo usuario valido"() {
        given: "Un nuevo usuario ingresado"
        this.service.registrarUsuario(UserDTO.builder().build()) >> { Respuesta.builder().mensaje(Constantes.Mensajes.EXITO).build() }
        when: "Cuando se ingresa un nuevo usuario"
        this.result = this.controller.registrarUsuario(UserDTO.builder().build())
        then: "Confirma el ingreso del usuario"
        result.statusCode == HttpStatus.OK

    }

    def "Registrar un nuevo usuario con correo invalido"() {
        given: "Un nuevo usuario ingresado"
        UserDTO user = getUserDTO();
        user.email = "bastian.com"
        this.service.registrarUsuario(user) >> { throw new BusinessException(Constantes.Mensajes.EMAIL_INCORRECTO) }
        when: "Cuando se ingresa un nuevo usuario con error"
        this.result = this.controller.registrarUsuario(user)
        then: "Confirma el ingreso del usuario"
        result.statusCode == HttpStatus.BAD_REQUEST

    }

    def "Registrar un nuevo usuario con contraseña invalida"() {
        given: "Un nuevo usuario ingresado"
        UserDTO user = getUserDTO();
        user.password = "bastian123"
        this.service.registrarUsuario(user) >> { throw new BusinessException(Constantes.Mensajes.EMAIL_INCORRECTO) }
        when: "Cuando se ingresa un nuevo usuario con error"
        this.result = this.controller.registrarUsuario(user)
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST

    }

    def "Modificar un usuario registrado"() {
        given: "Un usuario modificado"
        this.service.modificarUsuario(UserDTO.builder().build(), "jwt") >> { Respuesta.builder().mensaje(Constantes.Mensajes.EXITO).build() }
        when: "Se ingresan nuevos datos para un usuario"
        this.result = this.controller.modificarUsuario(UserDTO.builder().build(), "jwt")
        then: "Confirma la modificacion del usuario"
        result.statusCode == HttpStatus.OK
    }

    def "Modificar usuario con JWT inválido"() {
        given: "Un usuario modificado"
        this.service.modificarUsuario(UserDTO.builder().build(), "jwt") >> { throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO) }
        when: "Se ingresan nuevos datos para un usuario"
        this.result = this.controller.modificarUsuario(UserDTO.builder().build(), "jwt")
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST
    }

    def "Obtener usuario"() {
        given: "Un usuario"
        this.service.obtenerUsuario("jwt") >> { User.builder().build() }
        when: "Se obtienen datos de un usuario"
        this.result = this.controller.obtenerDataUsuario("jwt")
        then: "Devuelve un usuario"
        result.statusCode == HttpStatus.OK
    }

    def "Obtener usuario con JWT inválido"() {
        given: "Un usuario"
        this.service.obtenerUsuario("jwt") >> { throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO) }
        when: "Se obtienen datos de un usuario"
        this.result = this.controller.obtenerDataUsuario("jwt")
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST
    }

    def "Desactivar usuario"() {
        given: "Un usuario"
        this.service.desactivarUsuario("jwt") >> { Respuesta.builder().mensaje(Constantes.Mensajes.EXITO).build() }
        when: "Se desactiva un usuario"
        this.result = this.controller.desactivarUsuario("jwt")
        then: "Confirma la operación"
        result.statusCode == HttpStatus.OK
    }


    def "Desactivar usuario con JWT inválido"() {
        given: "Un usuario"
        this.service.desactivarUsuario("jwt") >> { throw new BusinessException(Constantes.Mensajes.TOKEN_INVALIDO) }
        when: "Se desactiva un usuario con jwt inválido"
        this.result = this.controller.desactivarUsuario("jwt")
        then: "Devuelve un error"
        result.statusCode == HttpStatus.BAD_REQUEST
    }


    Phone getPhone() {
        return Phone.builder()
                .number(31950150)
                .citycode(9)
                .countrycode(56)
                .build();
    }


    List<Phone> getPhoneList() {
        List<Phone> phoneList = new ArrayList<Phone>();
        phoneList.add(getPhone());
        return phoneList;
    }


    UserDTO getUserDTO() {
        return UserDTO.builder()
                .name(name)
                .phones(getPhoneList())
                .email(email)
                .password(password)
                .build();
    }


}
